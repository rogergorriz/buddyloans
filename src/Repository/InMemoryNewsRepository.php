<?php

namespace BuddyLoansTest\Repository;

use BuddyLoansTest\Entity\News;

class InMemoryNewsRepository implements NewsRepository
{

    private $news;

    public function __construct()
    {
        $this->news = [];
    }

    public function addNew(News $new)
    {
        $this->news[] = $new;
    }

    public function getAllNews()
    {
        return $this->news;
    }
}