<?php

namespace BuddyLoansTest\Repository;

use BuddyLoansTest\Entity\News;
use BuddyLoansTest\Infrastructure\SQLiteConnection;
use PDO;

class SQLiteNewsRepository implements NewsRepository
{

    private $pdo;

    public function __construct()
    {
        $this->pdo = (new SQLiteConnection())->connect();
    }

    public function getAllNews()
    {
        $query = $this->pdo->query('SELECT * FROM news ORDER BY date DESC');
        $query->execute();
        $results = $query->fetchAll(PDO::FETCH_OBJ);
        return $results;
    }
}