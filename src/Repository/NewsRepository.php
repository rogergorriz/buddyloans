<?php

namespace BuddyLoansTest\Repository;

use BuddyLoansTest\Infrastructure\SQLiteConnection;

interface NewsRepository
{
    public function getAllNews();
}