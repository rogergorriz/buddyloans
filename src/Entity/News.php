<?php

namespace BuddyLoansTest\Entity;
/**
 * To be mapped by an ORM.
 */
class News
{
    private $header;
    private $date;
    private $content;

    public function __construct($header, $date, $content)
    {
        $this->header = $header;
        $this->date = $date;
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }
}