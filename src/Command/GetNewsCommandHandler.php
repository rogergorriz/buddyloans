<?php

namespace BuddyLoansTest\Command;

use BuddyLoansTest\Repository\NewsRepository;

class GetNewsCommandHandler
{
    /**
     * @var NewsRepository
     */
    private $newsRepository;

    public function __construct(NewsRepository $newsRepository)
    {

        $this->newsRepository = $newsRepository;
    }

    public function handle(GetNewsCommand $command)
    {
        return $this->newsRepository->getAllNews();
    }
}