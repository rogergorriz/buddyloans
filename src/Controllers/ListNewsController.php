<?php

namespace BuddyLoansTest\Controllers;

use BuddyLoansTest\Command\GetNewsCommand;
use BuddyLoansTest\Command\GetNewsCommandHandler;
use BuddyLoansTest\Repository\SQLiteNewsRepository;

class ListNewsController
{
    public function __construct()
    {
        $command = new GetNewsCommand();
        $commandHandler = new GetNewsCommandHandler(new SQLiteNewsRepository());

        $news = $commandHandler->handle($command);

        var_dump($news);
    }
}