<?php

use BuddyLoansTest\Command\GetNewsCommand;
use BuddyLoansTest\Command\GetNewsCommandHandler;
use BuddyLoansTest\Entity\News;
use BuddyLoansTest\Repository\InMemoryNewsRepository;
use PHPUnit\Framework\TestCase;

class GetNewsCommandHandlerTest extends TestCase
{
    /** @var InMemoryNewsRepository */
    private $newsRepository;
    /** @var  GetNewsCommandHandler */
    private $commandHandler;

    public function setUp()
    {
        $this->newsRepository = new InMemoryNewsRepository();
        $this->commandHandler = new GetNewsCommandHandler($this->newsRepository);
    }

    public function testWhenThereAreAnyNewsWeShouldRetrieveIt()
    {
        $this->whenWeHaveAnyNews();

        $command = new GetNewsCommand();
        $news = $this->commandHandler->handle($command);

        $this->assertNotEmpty($news);
    }

    public function testWhenThereAreNotNewsWeShouldRetrieveEmptyArray()
    {
        $this->whenWeDontHaveAnyNews();

        $command = new GetNewsCommand();
        $news = $this->commandHandler->handle($command);

        $this->assertEmpty($news);
    }

    private function whenWeHaveAnyNews()
    {
        $this->newsRepository->addNew(new News('Fake new', new DateTime(), 'Fake news are fake news'));
    }

    private function whenWeDontHaveAnyNews()
    {

    }
}